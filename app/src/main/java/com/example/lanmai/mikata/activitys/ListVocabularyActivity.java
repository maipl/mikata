package com.example.lanmai.mikata.activitys;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;

import com.example.lanmai.mikata.R;
import com.example.lanmai.mikata.adapters.DataAdapter;
import com.example.lanmai.mikata.adapters.ListVocabularyAdapter;
import com.example.lanmai.mikata.fragments.VocabularyFragment;
import com.example.lanmai.mikata.models.Level;
import com.example.lanmai.mikata.models.Vocabulary;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

/**
 * Created by Lan Mai on 2/22/2017.
 */
public class ListVocabularyActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ListVocabularyAdapter adapter;
    private List<Vocabulary> mListVocabulary;
    private Level mLevel;
    private DataAdapter mDbHelper;
    private VocabularyFragment mVocabularyFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_vocabulary);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mLevel = (Level) getIntent().getExtras().getSerializable("level");

        initCollapsingToolbar();

        mDbHelper = new DataAdapter(this.getApplicationContext());
        mDbHelper.createDatabase();
        mDbHelper.open();
        mListVocabulary = mDbHelper.getVocabularyByLevel(mLevel);

        initRecyclerViewVocabulary();
        initFab();

        mDbHelper.close();
    }

    /**
     * Initializing RecyclerView
     * Will show list Vocabulary
     */
public void initRecyclerViewVocabulary(){
    recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
    recyclerView.setLayoutManager(mLayoutManager);
    recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
    recyclerView.setItemAnimator(new SlideInUpAnimator());

    adapter = new ListVocabularyAdapter(this, mListVocabulary);
    recyclerView.setAdapter(adapter);
    adapter.setOnItemClickListener(new ListVocabularyAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            Vocabulary vocabulary = mListVocabulary.get(position);
            showVocabularyActivity(vocabulary);
        }
    });
}
    /**
     * Initializing Floating Button
     * Will show action flash card, test...
     */
    public void initFab(){
        FloatingActionMenu fab = (FloatingActionMenu) findViewById(R.id.fab);
        // add button flashcard when fab was clicked
        FloatingActionButton fabFlashCard = new FloatingActionButton(this);
        fabFlashCard.setLabelText("study with flash card");
        fabFlashCard.setImageResource(R.drawable.ic_menu_send);
        fab.addMenuButton(fabFlashCard);
        fabFlashCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(ListVocabularyActivity.this, " was clicked!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ListVocabularyActivity.this, FlashCardActivity.class);
                String message = String.valueOf(mLevel.getId());
                intent.putExtra("id_level", message);
                startActivity(intent);
            }
        });
        FloatingActionButton fabTest = new FloatingActionButton(this);
        fabTest.setLabelText("test with multiple choice");
        fabTest.setImageResource(R.drawable.ic_import_contacts);
        fab.addMenuButton(fabTest);
    }

    /**
     * start activity show vocabulary
     * @param vocabulary
     */
    public void showVocabularyActivity(Vocabulary vocabulary) {
        Intent intent = new Intent(this, VocabularyActivity.class);
        String message = String.valueOf(vocabulary.getId());
        intent.putExtra("id_vocabulary", message);
        startActivity(intent);
    }

    public void showFragment(Vocabulary vocabulary) {
        mVocabularyFragment = new VocabularyFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.main_content, mVocabularyFragment.newInstance(vocabulary.getId()));
        ft.commit();
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        if (collapsingToolbar != null) {
            collapsingToolbar.setTitle(mLevel.getLevel_text());
        }
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        if (appBarLayout != null) {
            appBarLayout.setExpanded(true);
        }

        // hiding & showing the title when toolbar expanded & collapsed
        if (appBarLayout != null) {
            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                boolean isShow = false;
                int scrollRange = -1;
                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    if (scrollRange == -1) {
                        scrollRange = appBarLayout.getTotalScrollRange();
                    }
                    if (scrollRange + verticalOffset == 0) {
                        collapsingToolbar.setTitle(mLevel.getLevel_text());
                        isShow = true;
                    } else if (isShow) {
                        collapsingToolbar.setTitle(mLevel.getLevel_text());
                        isShow = false;
                    }
                }
            });
        }
    }


    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}