package com.example.lanmai.mikata.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.lanmai.mikata.R;
import com.example.lanmai.mikata.adapters.DataAdapter;
import com.example.lanmai.mikata.dialogs.AddExampleDialogFragment;
import com.example.lanmai.mikata.models.Vocabulary;

import butterknife.ButterKnife;

/**
 * Created by Lan Mai on 2/24/2017.
 */
public class VocabularyFragment extends Fragment {
    private static Vocabulary mVocabulary;
    private static int mId;
    private TextView tvKanji;
    private TextView tvKana;
    private TextView tvVietnamese;
    private TextView tvExamle;
    private Button btnAddExample;
    private Button btnEditExample;

    public static VocabularyFragment newInstance(int id) {
        VocabularyFragment newVocabularyFragment = new VocabularyFragment();
        mId = id;
        return newVocabularyFragment;
    }

    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_vocabulary, container, false);
        ButterKnife.bind(this, view);
        DataAdapter mDbHelper = new DataAdapter(getActivity().getApplicationContext());
        mDbHelper.createDatabase();
        mDbHelper.open();
        mVocabulary = mDbHelper.getVocabularyByID(mId);
        initToolbar();
        initData(view);
        mDbHelper.close();
        return view;
    }

    public Toolbar initToolbar() {
        AppCompatActivity mAppCompatActivity = (AppCompatActivity) getActivity();
        Toolbar toolbar = (Toolbar) mAppCompatActivity.findViewById(R.id.toolbar);
        mAppCompatActivity.setSupportActionBar(toolbar);
        ActionBar actionBar = mAppCompatActivity.getSupportActionBar();
        actionBar.setTitle(mVocabulary.getKanji());
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        return toolbar;
    }
    private void initData(View view){
        tvKanji = (TextView) view.findViewById(R.id.kanji);
        tvKana = (TextView) view.findViewById(R.id.kana);
        tvVietnamese = (TextView) view.findViewById(R.id.vietnamese);
        tvExamle = (TextView) view.findViewById(R.id.example);
        btnAddExample = (Button) view.findViewById(R.id.btn_add_example);
        btnEditExample = (Button) view.findViewById(R.id.btn_edit_example);

        tvKanji.setText(mVocabulary.getKanji());
        tvKana.setText(mVocabulary.getKana());
        tvVietnamese.setText(mVocabulary.getVietnamese());
        tvExamle.setText(mVocabulary.getExample());

        btnAddExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                FragmentManager fm = getFragmentManager();
                AddExampleDialogFragment dialogFragment = new AddExampleDialogFragment().newInstance(mVocabulary,2);
                dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {

                    }
                });
//                dialogFragment.show(fm, "sometag");
                dialogFragment.show(fm, "dialog_add_example");
//                fm.detach(this).attach(this).commit();
            }
        });

        btnEditExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                AddExampleDialogFragment dialogFragment = new AddExampleDialogFragment().newInstance(mVocabulary,1);
                dialogFragment.show(fm, "dialog_add_example");
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();

    }

}

