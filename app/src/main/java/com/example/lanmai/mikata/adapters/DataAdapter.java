package com.example.lanmai.mikata.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.lanmai.mikata.commons.ConstantTable;
import com.example.lanmai.mikata.databases.DatabaseHelper;
import com.example.lanmai.mikata.models.Level;
import com.example.lanmai.mikata.models.Vocabulary;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lan Mai on 2/18/2017.
 */
public class DataAdapter {
    protected static final String TAG = "DataAdapter";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    public DataAdapter(Context context) {
        this.mContext = context;
        mDbHelper = new DatabaseHelper(mContext);
    }

    public DataAdapter createDatabase() throws SQLException {
        try {
            mDbHelper.createDataBase();
        } catch (IOException mIOException) {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    public DataAdapter open() throws SQLException {
        try {
            mDbHelper.openDataBase();
            mDbHelper.close();
//            mDb = mDbHelper.getWritableDatabase();
            mDb = mDbHelper.getReadableDatabase();
        } catch (SQLException mSQLException) {
            Log.e(TAG, "open >>" + mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public List<Level> getAllLevel() {
        String sql = " SELECT * FROM " + ConstantTable.TABLE_LEVEL;
//        Level level = null;
        List<Level> listLevel = new ArrayList<>();
        open();
        Cursor cursor = mDb.rawQuery(sql, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Level level = new Level();
            level.setId(cursor.getInt(cursor.getColumnIndex(ConstantTable.ID)));
            level.setLevel_text(cursor.getString(cursor.getColumnIndex(ConstantTable.Level.COL_LEVEL_TEXT)));
            level.setLevel_number(cursor.getInt(cursor.getColumnIndex(ConstantTable.Level.COL_LEVEL_NUMBER)));
//            Log.d("AAA", level.getLevel_text()+"");
            listLevel.add(level);
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return listLevel;
    }

    public List<Vocabulary> getVocabularyByLevel(Level level) {
        Log.d("AA", level.getLevel_number() + "");
        String sql = " SELECT * FROM " + ConstantTable.TABLE_VOCABULARY + " WHERE level=" + level.getLevel_number();
//        Vocabulary vocabulary=null;
        List<Vocabulary> listVocabulary = new ArrayList<>();
        open();
        Cursor cursor = mDb.rawQuery(sql, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Vocabulary vocabulary = new Vocabulary();
            vocabulary.setId(cursor.getInt(cursor.getColumnIndex(ConstantTable.ID)));

            vocabulary.setLevel(cursor.getInt(cursor.getColumnIndex(ConstantTable.Vocabulary.COL_LEVEL)));
            vocabulary.setKanji(cursor.getString(cursor.getColumnIndex(ConstantTable.Vocabulary.COL_KANJI)));
            vocabulary.setKana(cursor.getString(cursor.getColumnIndex(ConstantTable.Vocabulary.COL_KANA)));
            vocabulary.setVietnamese(cursor.getString(cursor.getColumnIndex(ConstantTable.Vocabulary.COL_VIETNAMESE)));
            vocabulary.setExample(cursor.getString(cursor.getColumnIndex(ConstantTable.Vocabulary.COL_EXAMPLE)));
            vocabulary.setCount(cursor.getInt(cursor.getColumnIndex(ConstantTable.Vocabulary.COL_COUNT)));
            vocabulary.setMark(cursor.getInt(cursor.getColumnIndex(ConstantTable.Vocabulary.COL_MARK)));
            listVocabulary.add(vocabulary);
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return listVocabulary;
    }

    public void updateExample(int id, String example) {
        open();
        ContentValues values = new ContentValues();
        values.put(ConstantTable.Vocabulary.COL_EXAMPLE, example);
        mDb.update(ConstantTable.TABLE_VOCABULARY, values, ConstantTable.ID + "=" + id, null);
        close();
    }

    public Vocabulary getVocabularyByID(int id) {
        String sql = " SELECT * FROM " + ConstantTable.TABLE_VOCABULARY + " WHERE " + ConstantTable.ID + "=" + id;
//        Vocabulary vocabulary=null;
        List<Vocabulary> listVocabulary = new ArrayList<>();
        open();
        Cursor cursor = mDb.rawQuery(sql, null);
        cursor.moveToFirst();

        Vocabulary vocabulary = new Vocabulary();
        vocabulary.setId(cursor.getInt(cursor.getColumnIndex(ConstantTable.ID)));

        vocabulary.setLevel(cursor.getInt(cursor.getColumnIndex(ConstantTable.Vocabulary.COL_LEVEL)));
        vocabulary.setKanji(cursor.getString(cursor.getColumnIndex(ConstantTable.Vocabulary.COL_KANJI)));
        vocabulary.setKana(cursor.getString(cursor.getColumnIndex(ConstantTable.Vocabulary.COL_KANA)));
        vocabulary.setVietnamese(cursor.getString(cursor.getColumnIndex(ConstantTable.Vocabulary.COL_VIETNAMESE)));
        vocabulary.setExample(cursor.getString(cursor.getColumnIndex(ConstantTable.Vocabulary.COL_EXAMPLE)));
        vocabulary.setCount(cursor.getInt(cursor.getColumnIndex(ConstantTable.Vocabulary.COL_COUNT)));
        vocabulary.setMark(cursor.getInt(cursor.getColumnIndex(ConstantTable.Vocabulary.COL_MARK)));
        listVocabulary.add(vocabulary);

        cursor.close();
        close();
        return vocabulary;
    }

    public Level getLevelById(int id) {
        String sql = " SELECT * FROM " + ConstantTable.TABLE_LEVEL + " WHERE " + ConstantTable.ID + "=" + id;
        open();
        Cursor cursor = mDb.rawQuery(sql, null);
        cursor.moveToFirst();
        Level level = new Level();
        level.setId(cursor.getInt(cursor.getColumnIndex(ConstantTable.ID)));
        level.setLevel_text(cursor.getString(cursor.getColumnIndex(ConstantTable.Level.COL_LEVEL_TEXT)));
        level.setLevel_number(cursor.getInt(cursor.getColumnIndex(ConstantTable.Level.COL_LEVEL_NUMBER)));
        return level;
    }
}