package com.example.lanmai.mikata.models;

import java.io.Serializable;

/**
 * Created by Lan Mai on 2/20/2017.
 */
@SuppressWarnings("serial")
public class Vocabulary implements Serializable {
    private int id;
    private int level;
    private String kanji;
    private String kana;
    private String vietnamese;
    private String example;
    private int count;
    private int mark;

    public Vocabulary(){}

    public Vocabulary(int id, int level, String kanji, String kana, String vietnamese, String example, int count, int mark) {
        this.id = id;
        this.level = level;
        this.kanji = kanji;
        this.kana = kana;
        this.vietnamese = vietnamese;
        this.example = example;
        this.count = count;
        this.mark = mark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getKanji() {
        return kanji;
    }

    public void setKanji(String kanji) {
        this.kanji = kanji;
    }

    public String getKana() {
        return kana;
    }

    public void setKana(String kana) {
        this.kana = kana;
    }

    public String getVietnamese() {
        return vietnamese;
    }

    public void setVietnamese(String vietnamese) {
        this.vietnamese = vietnamese;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
}
