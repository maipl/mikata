package com.example.lanmai.mikata.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.example.lanmai.mikata.R;
import com.example.lanmai.mikata.adapters.DataAdapter;
import com.example.lanmai.mikata.models.Vocabulary;

/**
 * Created by Lan Mai on 2/24/2017.
 */
public class AddExampleDialogFragment extends DialogFragment {
    private EditText textExample;
    private static Vocabulary mVocabulary;
    private static int mFlag;
    private DialogInterface.OnDismissListener onDismissListener;
//    private FeedbackListener feedbackListener;

    //if edit then flag=1;
    public static AddExampleDialogFragment newInstance(Vocabulary vocabulary, int flag) {
        AddExampleDialogFragment newAddExampleDialogFragment = new AddExampleDialogFragment();
        mVocabulary = vocabulary;
        mFlag = flag;
        return newAddExampleDialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.setContentView(R.layout.dialog_add_example);
        final DataAdapter mDbHelper = new DataAdapter(getActivity().getApplicationContext());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDbHelper.createDatabase();
        mDbHelper.open();
        textExample = (EditText) dialog.findViewById(R.id.example);
        if (mFlag==1) textExample.setText(mVocabulary.getExample());

        // OK
        dialog.findViewById(R.id.positive_button).setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                String example = null;
                switch (mFlag){
                    case 1: //edit
                        example = textExample.getText().toString();
                        break;
                    case 2: //add
                        if(mVocabulary.getExample()==null){
                            example = textExample.getText().toString();
                        } else
                            example = mVocabulary.getExample() + "\n" + textExample.getText().toString();
                        break;
                }
                mDbHelper.updateExample(mVocabulary.getId(), example);
                dismiss();
            }
        });

        // Close
        dialog.findViewById(R.id.close_button).setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                dismiss();
            }

        });
        mDbHelper.close();
        return dialog;
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }
}