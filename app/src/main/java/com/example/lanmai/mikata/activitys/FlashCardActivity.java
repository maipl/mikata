package com.example.lanmai.mikata.activitys;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.lanmai.mikata.R;
import com.example.lanmai.mikata.adapters.CardAdapter;
import com.example.lanmai.mikata.adapters.DataAdapter;
import com.example.lanmai.mikata.models.Level;
import com.example.lanmai.mikata.models.Vocabulary;

import java.util.List;

public class FlashCardActivity extends AppCompatActivity {
    private DataAdapter mDbHelper;
    private ViewPager mViewPager;
    private List<Vocabulary> mListVocabulary;
    private Level mLevel;
    private int mIdLevel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash_card);
        String id_str = getIntent().getStringExtra("id_vocabulary");
        mIdLevel = Integer.valueOf(id_str);
        mLevel = mDbHelper.getLevelById(mIdLevel);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mDbHelper = new DataAdapter(this.getApplicationContext());
        mDbHelper.createDatabase();
        mDbHelper.open();
        mListVocabulary = mDbHelper.getVocabularyByLevel(mLevel);



        CardAdapter cardAdapter =new CardAdapter(getSupportFragmentManager(),3);
        mViewPager.setAdapter(cardAdapter);

        mDbHelper.close();
    }
}