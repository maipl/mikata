package com.example.lanmai.mikata.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.lanmai.mikata.R;
import com.example.lanmai.mikata.models.Level;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lan Mai on 2/20/2017.
 */
public class ListLevelAdapter extends ArrayAdapter<Level> {
    private List<Level> mArrayList;
    private Activity mContext;
    int layoutId;
    static class ViewHolder {
        @BindView(R.id.textLevelIcon) TextView tvLevelNumber;
        @BindView(R.id.textLevelName) TextView tvLevelName;

        public ViewHolder(View view){
            ButterKnife.bind(this, view);
        }
    }
    public ListLevelAdapter(Activity context, List<Level> list){
        super(context,R.layout.item_list_level,list);
        this.mContext = context;
//        this.layoutId = layoutId;
        this.mArrayList = list;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater inflater = mContext.getLayoutInflater();
//        convertView = inflater.inflate(layoutId, null);
            convertView = inflater.inflate(R.layout.item_list_level, parent, false);
            TextView tvLevelNumber = (TextView)convertView.findViewById(R.id.textLevelIcon);
            TextView tvLevelName = (TextView)convertView.findViewById(R.id.textLevelName);

//            if(position>0) {
            Level level = this.mArrayList.get(position);
            tvLevelNumber.setText(level.getLevel_number()+"");
            tvLevelName.setText(level.getLevel_text());
//            }

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        return convertView;
    }
}

