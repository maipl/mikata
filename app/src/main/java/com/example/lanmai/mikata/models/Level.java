package com.example.lanmai.mikata.models;

import java.io.Serializable;

/**
 * Created by Lan Mai on 2/20/2017.
 */
@SuppressWarnings("serial")
public class Level implements Serializable {
    private int id;
    private int level_number;
    private String level_text;

    public Level() {}

    public Level(int id, int level_number, String level_text) {
        this.id = id;
        this.level_number = level_number;
        this.level_text = level_text;
    }

    public Level(int level_number, String level_text) {
        this.level_number = level_number;
        this.level_text = level_text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLevel_number() {
        return level_number;
    }

    public void setLevel_number(int level_number) {
        this.level_number = level_number;
    }

    public String getLevel_text() {
        return level_text;
    }

    public void setLevel_text(String level_text) {
        this.level_text = level_text;
    }
}
