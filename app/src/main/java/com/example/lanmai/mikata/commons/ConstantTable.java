package com.example.lanmai.mikata.commons;

/**
 * Created by Lan Mai on 2/20/2017.
 */
public class ConstantTable {
    public static final String DB_BACKUP = "backup.sqlite";
    public static final String DB_NAME = "mimikaran1.sqlite";
    public static final String TABLE_BACKUP = "backup";
    public static final String TABLE_LEVEL = "level";
    public static final String TABLE_VOCABULARY = "vocabulary";
    public static final String ID = "id";


    public class Level {
        public static final String COL_LEVEL_TEXT = "level_text";
        public static final String COL_LEVEL_NUMBER = "level_number";

        public Level() {
        }
    }

    public class Vocabulary {
        public static final String COL_KANJI = "kanji";
        public static final String COL_LEVEL = "level";
        public static final String COL_KANA = "kana";
        public static final String COL_VIETNAMESE = "vietnamese";
        public static final String COL_EXAMPLE = "example";
        public static final String COL_COUNT = "count";
        public static final String COL_MARK = "mark";
        public Vocabulary() {
        }
    }
}
