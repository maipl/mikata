package com.example.lanmai.mikata.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lanmai.mikata.R;
import com.example.lanmai.mikata.models.Vocabulary;

import java.util.List;

/**
 * Created by Lan Mai on 2/22/2017.
 */
public class ListVocabularyAdapter extends RecyclerView.Adapter<ListVocabularyAdapter.MyViewHolder> {
    private Context mContext;
    private List<Vocabulary> mVocabularyList;

    /***** Creating OnItemClickListener *****/

    // Define listener member variable
    private OnItemClickListener listener;
    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvKanji, tvKanna, tvVietnamese;

        public MyViewHolder(View view) {
            super(view);
            tvKanji = (TextView) view.findViewById(R.id.kanji);
            tvKanna = (TextView) view.findViewById(R.id.kana);
            tvVietnamese = (TextView) view.findViewById(R.id.vietnamese);
            // Setup the click listener
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(itemView, position);
                        }
                    }
                }
            });
        }
    }


    public ListVocabularyAdapter(Context mContext, List<Vocabulary> vocabularyList) {
        this.mContext = mContext;
        this.mVocabularyList = vocabularyList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_vocabulary, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Vocabulary vocabulary = mVocabularyList.get(position);
        holder.tvKanji.setText(vocabulary.getKanji());
        holder.tvKanna.setText(vocabulary.getKana());
        holder.tvVietnamese.setText(vocabulary.getVietnamese());
    }



    @Override
    public int getItemCount() {
        return mVocabularyList.size();
    }
}
