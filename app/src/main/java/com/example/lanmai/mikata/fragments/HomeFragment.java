package com.example.lanmai.mikata.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.lanmai.mikata.R;
import com.example.lanmai.mikata.activitys.ListVocabularyActivity;
import com.example.lanmai.mikata.adapters.DataAdapter;
import com.example.lanmai.mikata.adapters.ListLevelAdapter;
import com.example.lanmai.mikata.models.Level;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Lan Mai on 2/18/2017
 */
public class HomeFragment extends Fragment {
    List<Level> mListLevel;
    ListView mListViewLevel;
    public static HomeFragment newInstance() {
        HomeFragment newHomeFragment = new HomeFragment();
        return newHomeFragment;
    }
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        DataAdapter mDbHelper = new DataAdapter(getActivity().getApplicationContext());
        mDbHelper.createDatabase();
        mDbHelper.open();

        mListLevel = mDbHelper.getAllLevel();
        mListViewLevel = (ListView) view.findViewById(R.id.list_level);
        ArrayAdapter<Level> adapter = new ListLevelAdapter(getActivity(), mListLevel);
        mListViewLevel.setAdapter(adapter);

        mListViewLevel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Level level = mListLevel.get(position);
                Intent intent = new Intent(getActivity(), ListVocabularyActivity.class);
                intent.putExtra("level", level);
                startActivity(intent);
//                FragmentManager mFragmentManager = getActivity().getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
////                fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
//                fragmentTransaction.replace(R.id.containerView, new SubjectContentFragment().newInstance(subject), "SubjectContentFragment");
//                fragmentTransaction.addToBackStack("SubjectContentFragment");
//                fragmentTransaction.commit();
            }
        });
        mDbHelper.close();
        return view;
    }
}
