package com.example.lanmai.mikata.activitys;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.lanmai.mikata.R;
import com.example.lanmai.mikata.adapters.DataAdapter;
import com.example.lanmai.mikata.dialogs.AddExampleDialogFragment;
import com.example.lanmai.mikata.models.Vocabulary;

public class VocabularyActivity extends AppCompatActivity {
    private static Vocabulary mVocabulary;
    private static int mId;
    private TextView tvKanji;
    private TextView tvKana;
    private TextView tvVietnamese;
    private TextView tvExamle;
    private Button btnAddExample;
    private Button btnEditExample;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vocabulary);
        String id_str = getIntent().getStringExtra("id_vocabulary");
        mId = Integer.valueOf(id_str);
        DataAdapter mDbHelper = new DataAdapter(this.getApplicationContext());
        mDbHelper.createDatabase();
        mDbHelper.open();
        mVocabulary = mDbHelper.getVocabularyByID(mId);
        initData();
    }

    private void initData(){
        tvKanji = (TextView) findViewById(R.id.kanji);
        tvKana = (TextView) findViewById(R.id.kana);
        tvVietnamese = (TextView) findViewById(R.id.vietnamese);
        tvExamle = (TextView) findViewById(R.id.example);
        btnAddExample = (Button) findViewById(R.id.btn_add_example);
        btnEditExample = (Button) findViewById(R.id.btn_edit_example);


        tvKanji.setText(mVocabulary.getKanji());
        tvKana.setText(mVocabulary.getKana());
        tvVietnamese.setText(mVocabulary.getVietnamese());
        tvExamle.setText(mVocabulary.getExample());

        btnAddExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                FragmentManager fm = getSupportFragmentManager();
                AddExampleDialogFragment dialogFragment = new AddExampleDialogFragment().newInstance(mVocabulary,2);
                dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {

                    }
                });
//                dialogFragment.show(fm, "sometag");
                dialogFragment.show(fm, "dialog_add_example");
//                fm.detach(this).attach(this).commit();
            }
        });

        btnEditExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                AddExampleDialogFragment dialogFragment = new AddExampleDialogFragment().newInstance(mVocabulary,1);
                dialogFragment.show(fm, "dialog_add_example");
            }
        });
    }
}
