package com.example.lanmai.mikata.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.lanmai.mikata.fragments.CardFragment;

/**
 * Created by Lan Mai on 2/25/2017.
 */
public class CardAdapter extends FragmentPagerAdapter {
    private int page_num;
    public CardAdapter(FragmentManager fm, int page_num) {
        super(fm);
        this.page_num = page_num;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return CardFragment.newInstant(0,"Home");
            case 1:
                return CardFragment.newInstant(1, "Friends");
            case 2:
                return CardFragment.newInstant(2, "Setting");
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return page_num;
    }
}
