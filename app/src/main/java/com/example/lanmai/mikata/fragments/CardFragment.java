package com.example.lanmai.mikata.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lanmai.mikata.R;

/**
 * Created by Lan Mai on 2/25/2017.
 */
public class CardFragment extends Fragment {

    private String text;
    private int page;

    public static CardFragment newInstant(int page, String text) {

        CardFragment frm_ = new CardFragment();
        Bundle argBundle = new Bundle();
        argBundle.putInt("page", page);
        argBundle.putString("content", text);
        frm_.setArguments(argBundle);
        return frm_;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("page");
        text = getArguments().getString("content");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_card, container, false);

        TextView view1 = (TextView) view.findViewById(R.id.tv);
        view1.setText(page + ", " + text);
        return view;
    }
}